import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyAYzkL_cY6jjVk0Nav2LUCfFcVMOK3a-dw",
    authDomain: "postsenthusiast.firebaseapp.com",
    databaseURL: "https://postsenthusiast.firebaseio.com",
    projectId: "postsenthusiast",
    storageBucket: "",
    messagingSenderId: "682366254888",
    appId: "1:682366254888:web:b8cad08f1df71c33"
  };

  firebase.initializeApp(firebaseConfig);
  firebase.firestore().settings({ timestampsInSnapshots: true });

  export default firebase;