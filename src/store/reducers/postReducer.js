import uuid from 'uuid';

const initState = {
    posts: [
        {
            id: uuid.v4(),
            title: 'Artificial Intelligence',
            content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            author: 'Test Account',
            createdAt: 'Wed Jan 16 2019 16:41:53 GMT+0500'
        },
        {
            id: uuid.v4(),
            title: 'Evolving World',
            content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            author: 'Tech Freak',
            createdAt: 'Thu Jun 06 2019 02:22:07 GMT+0500'
        },
        {
            id: uuid.v4(),
            title: 'Revolution at its peak!',
            content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            author: 'Maha Aziz',
            createdAt: 'Tue Jul 16 2019 13:18:56 GMT+0500'
        }
    ]
}

const postReducer = (state = initState, action) => {
    switch(action.type) {
        case 'CREATE_POST':
            Object.assign(action.post, {
                id: uuid.v4(),
                createdAt: new Date().toString()
            });
            state.posts.push(action.post);
            return state;
        default:
            return state;
    }
}

export default postReducer;