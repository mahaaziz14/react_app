const initState = {
    authError: null,
    signUpError: null
}

const authReducer = (state = initState, action) => {
    switch(action.type) {
        case 'LOGIN_ERROR':
            return {
                ...state,
                authError: 'Login Failed'
            }
        case 'LOGIN_SUCCESS':
            return {
                ...state,
                authError: null
            }
        case 'SIGNOUT_SUCCESS':
            return state;
        case 'SIGNUP_SUCCESS':
            return {
                ...state,
                signUpError: null
            }
        case 'SIGNUP_ERROR':
            console.log('Signup failed');
            return {
                ...state,
                signUpError: action.err.message
            }
        default:
            return state;
    }
}

export default authReducer;