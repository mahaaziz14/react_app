import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import moment from 'moment';

const PostDetails = (props) => {
    const { post, auth } = props;
    if(!auth.uid) {
        return <Redirect to="/signin" />
    }
    if(post) {
        return (
            <div className="container section post-details">
                <div className="card z-depth-0">
                    <div className="card-content">
                        <span className="card-title">{ post.title }</span>
                        <p>{ post.content }</p>
                    </div>
                    <div className="card-action grey lighten-4 grey-text">
                        <div>Posted by { post.author }</div>
                        <div>{ moment(post.createdAt).calendar() }</div>
                    </div>
                </div>
            </div>
        );
    } else {
        return(
            <div className="container center">
                <p>Oops...Post not found!</p>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => { 
    const id = ownProps.match.params.id;
    const posts = state.postReducer.posts;
    const post = posts ? posts.find(post => post.id === id) : null;
    return {
        post,
        auth: state.firebaseReducer.auth
    }
}

export default connect(mapStateToProps)(PostDetails);
