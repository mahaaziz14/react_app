import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createPost } from '../../store/actions/postActions';
import { Redirect } from 'react-router-dom';

class CreatePost extends Component {
    state = {
        title: '',
        content: '',
        author: ''
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value,
            author: this.props.author
        });
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.createPost(this.state);
        this.props.history.push('/');
    }
    render() {
        const { auth } = this.props;
        if(!auth.uid) {
            return <Redirect to="/signin" />
        }
        return (
            <div className="container">
                <form onSubmit={this.handleSubmit} className="white">
                    <h5 className="grey-text text-darken-3">Create new post</h5>
                    <div className="input-field">
                        <label htmlFor="title">Title</label>
                        <input type="text" id="title" onChange={this.handleChange} />
                    </div>
                    <div className="input-field">
                        <label htmlFor="content">Post</label>
                        <textarea id="content" className="materialize-textarea" cols="12"
                        rows="6" onChange={this.handleChange}></textarea>
                    </div>
                    <div className="input-field">
                        <button className="btn orange lighten-1 z-depth-0">Create</button>
                    </div>
                </form>
                
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const profile = state.firebaseReducer.profile;
    return {
        auth: state.firebaseReducer.auth,
        author: `${profile.firstName} ${profile.lastName}`
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createPost: (post) => { dispatch(createPost(post)); }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreatePost);
