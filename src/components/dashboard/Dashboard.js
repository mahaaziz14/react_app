import React, { Component } from 'react';
import PostList from '../posts/PostList';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

class Dashboard extends Component {
    render() {
        const { posts, auth } = this.props;
        if(!auth.uid) {
            return <Redirect to="/signin" />
        }
        return(
            <div className="dashboard container">
                <div className="col">
                    <div className="col s12 m6">
                        <PostList posts={posts} />
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        posts: state.postReducer.posts,
        auth: state.firebaseReducer.auth
    }
}

export default connect(mapStateToProps)(Dashboard);