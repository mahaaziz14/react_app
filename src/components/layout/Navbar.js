import React from 'react';
import { Link } from 'react-router-dom';
import SignedIn from './SignedIn';
import SignedOut from './SignedOut';
import { connect } from 'react-redux';

const Navbar = (props) => {
    const { auth, profile } = props;
    const links = auth.uid ? <SignedIn profile={profile} /> : <SignedOut />
    return (
        <nav className="nav-wrapper orange darken-3">
            <div className="container">
                <Link to="/" className="brand-logo">Posts Enthusiast</Link>
                { links }
            </div>
        </nav>
    );
}

const mapStateToProps = (state) => {
    console.log(state)
    return {
        auth: state.firebaseReducer.auth,
        profile: state.firebaseReducer.profile
    }
}

export default connect(mapStateToProps)(Navbar);